package com.oknow.start;

public class StartCommandServiceLayer
{
    private OsLayer ol;

    public void setOsLayer(OsLayer ol)
    { this.ol = ol;
    }

    public void serviceStart()
    {
        ol.orderingCommand("service docker start");
    }
    public void serviceStop()
    {
        ol.orderingCommand("service docker stop");
    }
    public String showImages()
    {
       return ol.orderingCommand("docker images");
    }
    public String showContainers()
    {
        return ol.orderingCommand("docker ps");
    }
}
