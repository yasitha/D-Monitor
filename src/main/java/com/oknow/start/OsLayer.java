package com.oknow.start;


import com.oknow.shellutill.ShellCommander;

public class OsLayer
{
    private ShellCommander sc;

    public void setShellCommander(ShellCommander sc)
    {   this.sc = sc;
    }

    public String orderingCommand(String command)
    {
        return sc.executeCommand(command);
    }
}
