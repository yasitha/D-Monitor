package com.oknow;

import com.oknow.shellutill.ShellCommander;
import com.oknow.start.OsLayer;
import com.oknow.start.StartCommandServiceLayer;

import javax.swing.*;

public class AppInitiator
{
    public static void main(String[] args)
    {
        App appController=new App();
        JFrame frame = new JFrame("D-Monitor");
        StartCommandServiceLayer startCommService = new StartCommandServiceLayer();
        OsLayer ol=new OsLayer();
        ShellCommander sc = new ShellCommander();

        appController.setFrame(frame);
        appController.setStartCommandServiceLayer(startCommService);
        startCommService.setOsLayer(ol);
        ol.setShellCommander(sc);

        appController.frameInitiator(appController);
    }
}
