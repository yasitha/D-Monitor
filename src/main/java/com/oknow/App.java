package com.oknow;

import com.oknow.start.StartCommandServiceLayer;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class App {

    private JFrame frame;
    private JPanel btn;//start
    private JPanel mainPanel;
    private JPanel btn2;//stop
    private JPanel images;
    private JPanel containers;
    private JTextArea textArea1;
    private StartCommandServiceLayer scsl;

    public void setStartCommandServiceLayer(StartCommandServiceLayer scsl)
    { this.scsl = scsl;
    }
    public void setFrame(JFrame frame)
    { this.frame = frame;
    }

    public App()
    {
        btn.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                scsl.serviceStart();
                //JOptionPane.showMessageDialog(null,"the message4");
            }
        });
        btn2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                scsl.serviceStop();
                //JOptionPane.showMessageDialog(null,"the message5");
            }
        });
        images.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                //super.mouseClicked(mouseEvent);
                textArea1.setText(scsl.showImages());
            }
        });
        containers.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                //super.mouseClicked(mouseEvent);
                textArea1.setText(scsl.showContainers());
            }
        });
    }


    /*//////////////////////////////////////////////////////////
            main metod is moved to the appInitiator class
    //////////////////////////////////////////////////////////*/

    public void frameInitiator(App appController)
    {
        frame.setContentPane(appController.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
